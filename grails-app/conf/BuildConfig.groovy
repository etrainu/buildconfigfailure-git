//grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
//grails.project.target.level = 1.6
//grails.project.source.level = 1.6

grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.source.level = 1.7
grails.project.target.level = 1.7
//grails.project.war.file = "target/${appName}-${appVersion}.war"

//Code Coverage Config
coverage {
	enabledByDefault = false
}

grails.project.dependency.resolver = "maven"
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
   repositories {
        inherits true // Whether to inherit repository definitions from plugins
		mavenLocal()
		mavenCentral()
		grailsPlugins()
        grailsHome()
        grailsCentral()

	    //CodeHaus.org is no longer resolving as of Aug 2015
		//mavenRepo "http://snapshots.repository.codehaus.org"
		//mavenRepo "http://repository.codehaus.org"
		//mavenRepo "http://download.java.net/maven/2/"
		//mavenRepo "http://repository.jboss.com/maven2/"
	}
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.
		compile "org.imgscalr:imgscalr-lib:4.2"
		
		compile "org.codehaus.groovy.modules.http-builder:http-builder:0.5.1", {
			excludes "commons-logging", "httpclient", "xml-apis", "groovy" }
		
		
		compile 'org.apache.httpcomponents:httpclient:4.3.5', {
			excludes "httpcore"
		}
		compile 'org.apache.httpcomponents:httpcore:4.3.2'
		
		compile "org.apache.commons:commons-lang3:3.3.2"
    }

    plugins {
		test ":code-coverage:1.2.7"
		build ":release:3.0.1", {
			excludes "rest-client-builder"
		}
		compile ":rest-client-builder:2.0.3"

		compile ':cache:1.1.1'
		compile ":cache-ehcache:1.0.1"
		
		compile ":aws-sdk:1.8.10.2" , {
			excludes "httpclient", "httpcore"
		}
		compile ":burning-image:0.5.1"
		compile ":cache-headers:1.1.5"
		
		// plugins for the build system only
		build ':tomcat:7.0.52.1'
		
		//Hibernate pluging is pulling it's own version of ehcache which was older 
		//than the one pulled by compile ":cache-ehcache:1.0.1"- http://jira.grails.org/browse/GPCACHEEHCACHE-10
		runtime ':hibernate:3.6.10.10',{
			excludes 'ehcache-core'
		}
    }
}

